package ru.t1.sarychevv.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;
import ru.t1.sarychevv.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager, @NotNull final Class<M> type) {
        super(entityManager, type);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @Nullable final List<M> models = findAll(userId);
        if (models == null) return;
        for (@NotNull final M model : models) {
            removeOne(model);
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getTableName().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, getTableName())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getTableName());
        @NotNull final Root<M> root = criteriaQuery.from(getTableName());
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getTableName().getSimpleName() + " m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getTableName())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId,
                            @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getTableName().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getTableName())
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getTableName().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final String userId, @NotNull final M model) throws Exception {
        entityManager.remove(model);
        return model;
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.merge(model);
    }

}

