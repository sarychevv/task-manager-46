package ru.t1.sarychevv.tm.repository.dto;

import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.api.service.dto.IProjectDTOService;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.api.service.dto.IUserDTOService;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.service.ConnectionService;
import ru.t1.sarychevv.tm.service.PropertyService;
import ru.t1.sarychevv.tm.service.dto.ProjectDTOService;
import ru.t1.sarychevv.tm.service.dto.TaskDTOService;
import ru.t1.sarychevv.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Category(DBCategory.class)
public class ProjectDTORepositoryTest extends TestCase {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @NotNull
    private static ProjectDTO USER_PROJECT1 = new ProjectDTO();

    @NotNull
    private static ProjectDTO USER_PROJECT2 = new ProjectDTO();

    @NotNull
    private static IProjectDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @Before
    public void setUp() throws Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setPassword("user_password");
        user.setLogin("user_login");
        user.setRole(Role.USUAL);
        user.setEmail("user_email");
        user.setFirstName("user_first_name");
        user.setLastName("user_last_name");
        user.setMiddleName("user_middle_name");
        userService.add(user);
        userId = user.getId();

        @NotNull final UserDTO admin = new UserDTO();
        admin.setPassword("admin_password");
        admin.setLogin("admin_login");
        admin.setRole(Role.ADMIN);
        admin.setEmail("admin_email");
        admin.setFirstName("admin_first_name");
        admin.setLastName("admin_last_name");
        admin.setMiddleName("admin_middle_name");
        userService.add(admin);
        adminId = admin.getId();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            @NotNull final ProjectDTO project_first = new ProjectDTO();
            project_first.setName("first_test_project_name");
            project_first.setDescription("first_test_project_description");
            USER_PROJECT1 = project_first;
            @NotNull final ProjectDTO project_second = new ProjectDTO();
            project_second.setName("first_test_project_name");
            project_second.setDescription("first_test_project_description");
            USER_PROJECT2 = project_second;
            entityManager.getTransaction().begin();
            repository.add(userId, project_first);
            repository.add(userId, project_second);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @After
    public void tearDown() throws Exception {
        @Nullable final UserDTO user = userService.findOneById(userId);
        if (user != null) userService.removeOne(user);

        @Nullable final UserDTO admin = userService.findOneById(adminId);
        if (admin != null) userService.removeOne(user);

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            repository.removeAll(adminId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @Test
    public void add() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            @NotNull final ProjectDTO project_third = new ProjectDTO();
            project_third.setName("third_test_project_name");
            project_third.setDescription("third_test_project_description");
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(userId, project_third));
            entityManager.getTransaction().commit();
            @Nullable final ProjectDTO project = repository.findOneById(userId, project_third.getId());
            Assert.assertNotNull(project);
            Assert.assertEquals(project_third.getId(), project.getId());
            Assert.assertEquals(userId, project.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(userId, UUID.randomUUID().toString()));
        entityManager.close();
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<ProjectDTO> projects = repository.findAll(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        entityManager.close();
    }

    @Test
    public void getSize() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(userId));
        entityManager.close();
    }

    @Test
    public void removeOne() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOne(userId, USER_PROJECT2);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_PROJECT2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void update() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            USER_PROJECT1.setName(USER_PROJECT2.getName());
            repository.update(USER_PROJECT1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_PROJECT2.getName(), repository.findOneById(userId, USER_PROJECT1.getId()).getName());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(userId, USER_PROJECT2));
            entityManager.getTransaction().commit();
            @Nullable final ProjectDTO project = repository.findOneById(userId, USER_PROJECT2.getId());
            Assert.assertNotNull(project);
            Assert.assertEquals(USER_PROJECT2.getId(), project.getId());
            Assert.assertEquals(userId, project.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testExistsById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(userId, UUID.randomUUID().toString()));
        Assert.assertTrue(repository.existsById(userId, USER_PROJECT1.getId()));
        entityManager.close();
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
        @Nullable final ProjectDTO project = repository.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
        entityManager.close();
    }

    @Test
    public void testGetSize() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(userId));
        entityManager.close();
    }

    @Test
    public void create() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTO project = repository.create(userId, USER_PROJECT2.getName());
            Assert.assertEquals(USER_PROJECT2.getName(), project.getName());
            Assert.assertEquals(userId, project.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
            Assert.assertEquals(repository.getSize(), 0);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }
}