package ru.t1.sarychevv.tm.dto.response;

import org.jetbrains.annotations.NotNull;

public class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
